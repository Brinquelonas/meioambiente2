﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DisclaimerMessage : MonoBehaviour {

	[System.Serializable]
	public class DisclaimerEvent : UnityEvent { }
	public DisclaimerEvent Event = new DisclaimerEvent();

	public Button OkButton;
	public Toggle ShowToggle;

	private static bool _notFirstTime;

	public bool DontShowAgain
	{
		get 
		{
			return PlayerPrefs.GetInt("DontShowDisclaimer") == 1;
		}
		set 
		{
			int val = 0;
			if (value)
				val = 1;

			PlayerPrefs.SetInt("DontShowDisclaimer", val);
		}
	}

	void Awake()
	{
		OkButton.onClick.AddListener(() => 
		{
			Event.Invoke();
		});

		ShowToggle.onValueChanged.AddListener((v) => 
		{
			SetShow(v);
		});
		
		if (DontShowAgain || _notFirstTime) 
		{
			Event.Invoke();
			return;
		}

		_notFirstTime = true;
	}

	void OnEnable()
	{
		ShowToggle.isOn = DontShowAgain;
	}

	public void SetShow(bool enable)
	{
		DontShowAgain = enable;
	}
}
