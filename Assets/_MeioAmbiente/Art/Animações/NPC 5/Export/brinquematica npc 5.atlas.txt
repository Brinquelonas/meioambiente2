
brinquematica npc 5.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
P L
  rotate: true
  xy: 2, 2
  size: 20, 24
  orig: 20, 24
  offset: 0, 0
  index: -1
P R
  rotate: false
  xy: 252, 174
  size: 12, 16
  orig: 12, 16
  offset: 0, 0
  index: -1
arm L
  rotate: true
  xy: 518, 354
  size: 156, 312
  orig: 156, 312
  offset: 0, 0
  index: -1
arm R 1
  rotate: false
  xy: 2, 24
  size: 184, 148
  orig: 184, 148
  offset: 0, 0
  index: -1
arm R 2
  rotate: true
  xy: 518, 280
  size: 72, 148
  orig: 72, 148
  offset: 0, 0
  index: -1
body 1
  rotate: true
  xy: 668, 276
  size: 76, 100
  orig: 76, 100
  offset: 0, 0
  index: -1
body 2
  rotate: false
  xy: 2, 174
  size: 248, 336
  orig: 248, 336
  offset: 0, 0
  index: -1
body 3
  rotate: false
  xy: 252, 192
  size: 224, 44
  orig: 224, 44
  offset: 0, 0
  index: -1
body_white mask
  rotate: false
  xy: 188, 52
  size: 36, 120
  orig: 36, 120
  offset: 0, 0
  index: -1
eyes 1
  rotate: false
  xy: 518, 234
  size: 92, 44
  orig: 92, 44
  offset: 0, 0
  index: -1
eyes 2
  rotate: true
  xy: 886, 274
  size: 96, 36
  orig: 96, 36
  offset: 0, 0
  index: -1
eyes 3
  rotate: false
  xy: 188, 2
  size: 96, 48
  orig: 96, 48
  offset: 0, 0
  index: -1
eyes 4
  rotate: true
  xy: 938, 414
  size: 96, 48
  orig: 96, 48
  offset: 0, 0
  index: -1
gola
  rotate: true
  xy: 226, 92
  size: 80, 40
  orig: 80, 40
  offset: 0, 0
  index: -1
gravata
  rotate: false
  xy: 832, 282
  size: 52, 88
  orig: 52, 88
  offset: 0, 0
  index: -1
hand R
  rotate: false
  xy: 832, 418
  size: 104, 92
  orig: 104, 92
  offset: 0, 0
  index: -1
hand R finger 1
  rotate: true
  xy: 832, 372
  size: 44, 104
  orig: 44, 104
  offset: 0, 0
  index: -1
hand R finger 2
  rotate: false
  xy: 988, 474
  size: 32, 36
  orig: 32, 36
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 252, 238
  size: 264, 272
  orig: 264, 272
  offset: 0, 0
  index: -1
head neck
  rotate: true
  xy: 770, 280
  size: 72, 60
  orig: 72, 60
  offset: 0, 0
  index: -1
head s L
  rotate: false
  xy: 938, 388
  size: 48, 24
  orig: 48, 24
  offset: 0, 0
  index: -1
head s R
  rotate: true
  xy: 28, 2
  size: 20, 12
  orig: 20, 12
  offset: 0, 0
  index: -1
