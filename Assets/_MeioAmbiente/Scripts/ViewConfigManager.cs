﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ViewConfigManager : MonoBehaviour {

    public Button GiantButton;
    public Button MegaButton;
    public Button BackButton;

    private void Start()
    {
        GiantButton.onClick.AddListener(() => 
        {
            SceneManager.LoadScene(6);
        });

        MegaButton.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(7);
        });

        BackButton.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(0);
        });
    }

}
