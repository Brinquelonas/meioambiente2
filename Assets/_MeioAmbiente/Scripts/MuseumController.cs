﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Spine.Unity;
using TMPro;

public class MuseumController : MonoBehaviour {

    [System.Serializable]
    public struct InteractionConfig
    {
        public string Tag;
        public Interaction Interaction;
    }

	public Image Balloon;
	public TextMeshProUGUI Question;
    public List<NPCController> NPCs = new List<NPCController>();
    public List<InteractionConfig> Interactions = new List<InteractionConfig>();
    public string ResourcesPath = "";

    //private bool _found = false;
    private Question _currentQuestion;

    public int Space { get; set; }
    public int CorrectAnswer { get; set; }

    public NPCController CurrentNPC
    {
        get
        {
            int index = Mathf.Clamp(_currentQuestion.NPCIndex - 1, 0, NPCs.Count);
            return NPCs[index];
        }
    }

	protected void Awake()
	{
        gameObject.SetActive(false);

		 
    }

    public void OnHandlerFoundInteraction(int space)
    {
        
        if (gameObject.activeSelf)
            return;

        gameObject.SetActive(true);

        Space = space;
        CorrectAnswer = Random.Range(0, 3);

        List<Question> questions = QuestionsTSVReader.Instance.GetInteractionsFromSpace(Space);
        int questionIndex = Random.Range(0, questions.Count);
        _currentQuestion = questions[questionIndex];

        //Question.text = _currentQuestion.Text;
        Balloon.gameObject.SetActive(false);

        CurrentNPC.gameObject.SetActive(true);
        PlaySpineAnimation("waiting", true);

        InitializeINTQuestion();
        
        PotaTween.Create(CurrentNPC.gameObject).Stop();
        PotaTween.Create(CurrentNPC.gameObject).
            SetAlpha(0f, 1f).
            SetDuration(0.5f).
            Play(() =>
            {
                PlaySpineAnimation("question", false, () =>
                {
                    PlaySpineAnimation("waiting", true);
                });
                ShowBalloon(() =>
                {
                    PlayAudio(_currentQuestion.Audio, () =>
                    {

                    });
                });
            });
    }

    public void Answer(int answer)
	{
		System.Action callback = () => 
		{
            HideAll();
		};

        if (answer == CorrectAnswer)
        {
            PlaySpineAnimation("right", false, callback);
            PlayAudio(null);
            Question.text = "Resposta correta!\n";            
        }
		else
        {
			PlaySpineAnimation("wrong", false, callback);
            PlayAudio(null);
            Question.text = "Que pena...\n";            
        }
	}
    
    private void InitializeINTQuestion()
    {
        for (int i = 0; i < Interactions.Count; i++)
        {
            if (Interactions[i].Tag == _currentQuestion.RightAnswer)
            {
                Interactions[i].Interaction.gameObject.SetActive(true);
                Interactions[i].Interaction.StartInteraction(Space);
                Interactions[i].Interaction.OnFinish.AddListener((b) =>
                {
                    if (b)
                        Answer(CorrectAnswer);
                    else
                        Answer(-1);
                    Interactions[i].Interaction.OnFinish.RemoveAllListeners();
                });

                PotaTween.Create(Interactions[i].Interaction.gameObject).
                    SetAlpha(0f, 1f).
                    SetDuration(0.5f).
                    Play();

                break;
            }
        }
    }

    private void PlayAudio(AudioClip clip, System.Action callback = null)
    {
        if (clip == null)
        {
            if (callback != null) callback();
            CurrentNPC.PlayMouthAnimation(2, () => CurrentNPC.PlayMouthAnimation(CurrentNPC.CurrentAnimationState));
            return;
        }

        AudioSource.PlayClipAtPoint(clip, Vector3.zero);

        if (callback != null)
            StartCoroutine(AudioCallback(clip, callback));
    }

    private void PlaySpineAnimation(string animationName, bool loop, System.Action callback = null)
    {
        CurrentNPC.PlayAnimation(animationName, loop, callback);
    }

    private void ShowBalloon(System.Action callback = null)
    {
        Balloon.gameObject.SetActive(true);

        PotaTween.Create(Balloon.gameObject).Stop();
        PotaTween.Create(Balloon.gameObject).
            SetAlpha(0f, 1f).
            SetScale( new Vector3(0.8f, 0.8f, 1), Vector3.one).
            SetDuration(0.5f).
            SetEaseEquation(Ease.Equation.OutBack).
            Play(callback);
    }


    private void HideAll()
    {
        HideElement(Balloon.gameObject);
        
            for (int i = 0; i < Interactions.Count; i++)
            {
                if (Interactions[i].Interaction.gameObject.activeSelf)
                {
                    Interaction interaction = Interactions[i].Interaction;
                    DisableInteraction(interaction);
                }
            }
        HideElement(CurrentNPC.gameObject, () => 
        {
            CurrentNPC.gameObject.SetActive(false);
            //GetComponent<Canvas>().enabled = false;
            //_found = false;
            gameObject.SetActive(false);
        });
    }

    private void DisableInteraction(Interaction interaction)
    {
        HideElement(interaction.gameObject, () =>
        {
            interaction.gameObject.SetActive(false);
        });
    }

    private void HideElement(GameObject element, System.Action callback = null)
    {
        PotaTween.Create(element, 1).Stop();
        PotaTween.Create(element, 1).
            SetAlpha(1f, 0f).
            SetDuration(0.5f).
            SetEaseEquation(Ease.Equation.OutSine).
            Play(() =>
            {
                if (callback != null)
                    callback();

                element.SetActive(false);
            });
    }

    private IEnumerator AudioCallback(AudioClip clip, System.Action callback)
    {
        float elapsedTime = 0;

        while(elapsedTime < clip.length)
        {
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        callback();
    }
}
