﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PhotoObjectButton : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public GameObject Prefab;
    public Transform Container;

    GameObject Obj { get; set; }

    public void OnBeginDrag(PointerEventData eventData)
    {
        Obj = Instantiate(Prefab, Container);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if(Obj != null)
            Obj.transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Obj = null;
    }

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}
}
