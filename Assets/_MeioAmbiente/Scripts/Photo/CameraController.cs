﻿using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour
{
    private static CameraController _instance;
    public static CameraController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<CameraController>();

            return _instance;
        }
    }

    public RawImage Image;

    public WebCamTexture CameraTexture { get; set; }

    void Start()
    {
        /*CameraTexture = new WebCamTexture(Screen.width, Screen.height);
        Image.texture = CameraTexture;
        Image.material.mainTexture = CameraTexture;

        CameraTexture.Play();*/
    }

    public void TakePhoto()
    {
        //CameraTexture.Pause();
        ScreenshotManager.SaveScreenshot("Screenshot_"+System.DateTime.Now.ToString(), Application.productName);

        Invoke("ResetCamera", 1f);
    }

    private void ResetCamera()
    {
        //CameraTexture.Play();
    }
}