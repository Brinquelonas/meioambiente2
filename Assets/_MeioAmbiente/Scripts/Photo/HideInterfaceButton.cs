﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HideInterfaceButton : MonoBehaviour {

    public PotaTween Tween;
    private bool _hidden;

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();

            return _button;
        }
    }

    void Awake()
    {
        Button.onClick.AddListener(HideShow);
    }

    void HideShow()
    {
        Tween.Stop();
        if (_hidden)
            Tween.Reverse(() => 
            {
                transform.localScale = new Vector3(1, -1, 1);
            });
        else
            Tween.Play(() => 
            {
                transform.localScale = Vector3.one;
            });

        _hidden = !_hidden;
    }
}
