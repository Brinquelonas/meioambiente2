﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class DraggableElement : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [System.Serializable]
    public class DragEvent : UnityEvent<DraggableElement> { }

    public bool LockX = false;
    public bool LockY = false;

    public DragEvent OnBegin = new DragEvent();
    public DragEvent OnDragging = new DragEvent();
    public DragEvent OnEnd = new DragEvent();
    public Vector3 StartPosition { get; set; }
    Transform startParent;

    private Vector3 _deltaPos;

    public void OnBeginDrag(PointerEventData eventData)
    {
        StartPosition = transform.position;
        _deltaPos = StartPosition - Input.mousePosition;
        OnBegin.Invoke(this);
    }

    public void OnDrag(PointerEventData eventData)
    {
        //_deltaPos = StartPosition - Input.mousePosition;
        Vector3 position = Input.mousePosition + _deltaPos;
        if (LockX)
            position.x = transform.position.x;
        if (LockY)
            position.y = transform.position.y;

        transform.position = position;
        OnDragging.Invoke(this);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        OnEnd.Invoke(this);
    }

}
