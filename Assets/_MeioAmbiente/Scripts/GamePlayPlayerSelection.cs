﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayPlayerSelection : MonoBehaviour {

    public QuizPlayer PlayerPrefab;
    public Transform ButtonsHolder;

    private List<Button> _buttons = new List<Button>();

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = GetComponent<PotaTween>();
            if (_tween == null)
                _tween = PotaTween.Create(gameObject).SetScale(Vector3.zero, Vector3.one).SetAlpha(0f, 1f).SetDuration(0.3f);

            return _tween;
        }
    }

    public void SetActive(bool active, System.Action callback = null)
    {
        if (active)
        {
            gameObject.SetActive(true);
            Tween.Stop();
            Tween.Play(callback);
        }
        else
        {
            Tween.Stop();
            Tween.Reverse(() => 
            {
                if (callback != null)
                    callback();

                gameObject.SetActive(false);
            });
        }
    }

    public void Initiatize(List<PlayerConfig> players, System.Action<string> buttonCallback)
    {
        if (_buttons.Count > 0)
        {
            for (int i = 0; i < _buttons.Count; i++)
            {
                _buttons[i].onClick.RemoveAllListeners();
                SetButtonListener(_buttons[i], players[i].Name, buttonCallback);
            }
            return;
        }

        for (int i = 0; i < players.Count; i++)
        {
            InitializeButton(players[i], buttonCallback);
        }
    }

    private void InitializeButton(PlayerConfig player, System.Action<string> callback)
    {
        QuizPlayer p = Instantiate(PlayerPrefab, ButtonsHolder);
        p.Initialize(player.Name, player.Number, player.Color);

        Button button = p.gameObject.AddComponent<Button>();
        button.onClick.AddListener(() => callback(p.Name));

        _buttons.Add(button);
    }

    private void SetButtonListener(Button button, string playerName, System.Action<string> callback)
    {
        button.onClick.AddListener(() => callback(playerName));
    }

    public void RemoveButton(string playerName)
    {
        for (int i = 0; i < _buttons.Count; i++)
        {
            if (_buttons[i].GetComponent<QuizPlayer>().Name == playerName)
            {
                Destroy(_buttons[i].gameObject);
                _buttons.RemoveAt(i);
                break;
            }
        }
    }

}
