﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasFollowObject : MonoBehaviour {

    public Transform Target;
    public Vector2 RelativePosition;

    private RectTransform _rectTransform;
    private RectTransform ThisRectTransform
    {
        get
        {
            if (_rectTransform == null)
                _rectTransform = GetComponent<RectTransform>();

            return _rectTransform;
        }
    }
    
    public RectTransform CanvasRectTransform;

    private void LateUpdate()
    {
        MoveToWorldPoint(Target.transform.position);
    }

    public void MoveToWorldPoint(Vector3 objectTransformPosition)
    {
        //Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, Target.position);
        //ThisRectTransform.anchoredPosition = screenPoint - CanvasRectTransform.sizeDelta / 2f + RelativePosition;

        Vector2 temp = Camera.main.WorldToViewportPoint(objectTransformPosition);

        //Calculate position considering our percentage, using our canvas size
        //So if canvas size is (1100,500), and percentage is (0.5,0.5), current value will be (550,250)
        temp.x *= CanvasRectTransform.sizeDelta.x;
        temp.y *= CanvasRectTransform.sizeDelta.y;

        //The result is ready, but, this result is correct if canvas recttransform pivot is 0,0 - left lower corner.
        //But in reality its middle (0.5,0.5) by default, so we remove the amount considering cavnas rectransform pivot.
        //We could multiply with constant 0.5, but we will actually read the value, so if custom rect transform is passed(with custom pivot) , 
        //returned value will still be correct.

        temp.x -= CanvasRectTransform.sizeDelta.x * CanvasRectTransform.pivot.x;
        temp.y -= CanvasRectTransform.sizeDelta.y * CanvasRectTransform.pivot.y;

        ThisRectTransform.anchoredPosition = temp + RelativePosition;
    }

}
