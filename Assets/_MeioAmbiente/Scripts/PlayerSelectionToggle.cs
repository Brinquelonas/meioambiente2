﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSelectionToggle : MonoBehaviour {

    public Text NumberText;
    public Color Color;
    public InputField NameInputField;

    private Toggle _toggle;
    public Toggle Toggle
    {
        get
        {
            if (_toggle == null)
                _toggle = GetComponent<Toggle>();

            return _toggle;
        }
    }

    private void Awake()
    {
        Toggle.onValueChanged.AddListener((isOn) => 
        {
            ToggleSelected(isOn);
        });

        ToggleSelected(Toggle.isOn);
    }

    public void ToggleSelected(bool isOn)
    {
        if (isOn)
        {
            NumberText.color = Color;
            NameInputField.gameObject.SetActive(true);
        }
        else
        {
            NumberText.color = Color.gray;
            NameInputField.gameObject.SetActive(false);
        }
    }

}
