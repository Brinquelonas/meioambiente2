﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlanetaryController : MonoBehaviour {

    public struct AstralBodyData
    {
        public string Name;
        public string Type;
        public string Radius;
        public List<string> Texts;

        public AstralBodyData(string name, string type, string radius, List<string> texts)
        {
            Name = name;
            Type = type;
            Radius = radius;
            Texts = texts;
        }
    }

    [System.Serializable]
    public struct PlanetsData
    {
        public GameObject GameObject;
        public TextMeshProUGUI PlanetName;
        public TextMeshProUGUI PlanetType;
        public TextMeshProUGUI PlanetRadius;
        public TextMeshProUGUI PlanetText;

        [Header("Planets")]
        public GameObject Sun;
        public GameObject Mercury;
        public GameObject Venus;
        public GameObject Earth;
        public GameObject Moon;
        public GameObject Mars;
        public GameObject Jupiter;
        public GameObject Saturn;
        public GameObject Uranus;
        public GameObject Neptune;
        public GameObject Pluto;
    }

    public GameObject RealScale;
    public GameObject IllustrativeScale;
    public GameObject PlanetaryCanvas;

    public TextAsset PlanetaryTSV;
    public Canvas[] NamesCanvas;
    public List<AstralBodyData> PlanetaryData;
    public PlanetsData DataDisplay;

    private int _infoIndex = 0;
    private List<string> _texts;

    [SerializeField]
    private float _planetsSpeed = 1f;
    public float PlanetsSpeed
    {
        get
        {
            return _planetsSpeed;
        }
        set
        {
            _planetsSpeed = value;

            SimpleViewer.rotateSpeed = _planetsSpeed;
        }
    }

    private void Start()
    {
        ReadPlanetaryTSV();
    }

    public void DrawOrbit()
    {
        SimpleSysObject.drawOrbit = !SimpleSysObject.drawOrbit;
    }

    public void DrawOrbit(UnityEngine.UI.Toggle toggle)
    {
        SimpleSysObject.drawOrbit = toggle.isOn;
    }

    public void SetSpeed(float speed)
    {
        SimpleViewer.timeScale = speed;
    }

    public void SetSpeed1(UnityEngine.UI.Toggle toggle)
    {
        if (toggle.isOn)
            SetSpeed(1);
    }

    public void SetSpeed100(UnityEngine.UI.Toggle toggle)
    {
        if (toggle.isOn)
            SetSpeed(100);
    }

    public void SetSpeed10000(UnityEngine.UI.Toggle toggle)
    {
        if (toggle.isOn)
            SetSpeed(10000);
    }

    public void SetSpeed1000000(UnityEngine.UI.Toggle toggle)
    {
        if (toggle.isOn)
            SetSpeed(1000000);
    }

    public void EnableNamesCanvas()
    {
        for (int i = 0; i < NamesCanvas.Length; i++)
        {
            NamesCanvas[i].gameObject.SetActive(!NamesCanvas[i].gameObject.activeSelf);
        }
    }

    public void EnableNamesCanvas(UnityEngine.UI.Toggle toggle)
    {
        for (int i = 0; i < NamesCanvas.Length; i++)
        {
            NamesCanvas[i].gameObject.SetActive(toggle.isOn);
        }
    }

    public void SetRealScale(UnityEngine.UI.Toggle toggle)
    {
        RealScale.SetActive(toggle.isOn);
        IllustrativeScale.SetActive(!toggle.isOn);
    }

    public void SetIllustrativeScale(UnityEngine.UI.Toggle toggle)
    {
        RealScale.SetActive(!toggle.isOn);
        IllustrativeScale.SetActive(toggle.isOn);
    }

    GameObject _lastActiveObject;
    public void OnPlanetClicked(SimpleSysObject planet)
    {
        if (!PlanetaryData.Exists((b) => b.Name == planet.Name))
            return;

        if (RealScale.activeSelf)
            _lastActiveObject = RealScale;
        else
            _lastActiveObject = IllustrativeScale;

        RealScale.SetActive(false);
        IllustrativeScale.SetActive(false);
        PlanetaryCanvas.SetActive(false);

        _infoIndex = 0;

        AstralBodyData data = PlanetaryData.Find((b) => b.Name == planet.Name);

        DataDisplay.GameObject.SetActive(true);
        DataDisplay.PlanetName.text = data.Name;
        DataDisplay.PlanetType.text = data.Type;
        DataDisplay.PlanetRadius.text = "Raio: " + data.Radius;
        DataDisplay.PlanetText.text = data.Texts[_infoIndex];

        _texts = data.Texts;

        DataDisplay.Sun.SetActive(false);
        DataDisplay.Mercury.SetActive(false);
        DataDisplay.Venus.SetActive(false);
        DataDisplay.Earth.SetActive(false);
        DataDisplay.Mars.SetActive(false);
        DataDisplay.Jupiter.SetActive(false);
        DataDisplay.Saturn.SetActive(false);
        DataDisplay.Uranus.SetActive(false);
        DataDisplay.Neptune.SetActive(false);
        DataDisplay.Pluto.SetActive(false);
        DataDisplay.Moon.SetActive(false);

        switch (data.Name)
        {
            default:
                break;

            case "Sol":
                DataDisplay.Sun.SetActive(true);
                break;

            case "Mercúrio":
                DataDisplay.Mercury.SetActive(true);
                break;

            case "Vênus":
                DataDisplay.Venus.SetActive(true);
                break;

            case "Terra":
                DataDisplay.Earth.SetActive(true);
                break;

            case "Marte":
                DataDisplay.Mars.SetActive(true);
                break;

            case "Júpiter":
                DataDisplay.Jupiter.SetActive(true);
                break;

            case "Saturno":
                DataDisplay.Saturn.SetActive(true);
                break;

            case "Urano":
                DataDisplay.Uranus.SetActive(true);
                break;

            case "Netuno":
                DataDisplay.Neptune.SetActive(true);
                break;

            case "Plutão":
                DataDisplay.Pluto.SetActive(true);
                break;

            case "Lua":
                DataDisplay.Moon.SetActive(true);
                break;
        }
    }

    public void NextPlanetInfo()
    {
        _infoIndex ++;
        _infoIndex %= _texts.Count;

        DataDisplay.PlanetText.text = _texts[_infoIndex];
    }

    public void CloseInfo()
    {
        DataDisplay.GameObject.SetActive(false);
        RealScale.SetActive(_lastActiveObject == RealScale);
        IllustrativeScale.SetActive(_lastActiveObject == IllustrativeScale);
        PlanetaryCanvas.SetActive(true);
    }

    public void ReadPlanetaryTSV()
    {
        if (PlanetaryTSV == null)
            return;

        PlanetaryData = new List<AstralBodyData>();

        string[] lines = PlanetaryTSV.text.Split(System.Environment.NewLine[0]);

        for (int i = 1; i < lines.Length; i++)
        {
            string[] contents = lines[i].Split("\t"[0]);

            string name = contents[0].Trim();

            string type = contents[1].Trim();

            string radius = contents[2].Trim();

            List<string> texts = new List<string>();
            for (int j = 0; j < 3; j++)
            {
                int index = 3 + j;
                if (!string.IsNullOrEmpty(contents[index].Trim()))
                    texts.Add(contents[index].Trim());
            }

            PlanetaryData.Add(new AstralBodyData(name, type, radius, texts));
        }
    }
}
