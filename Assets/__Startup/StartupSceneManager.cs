﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartupSceneManager : MonoBehaviour {

	public Button QuizButton;
	public Button ARButton;
	public Button ViewModeButton;
    public Button QuitButton;

	void Start()
	{
		ARButton.onClick.AddListener(() =>
		{
			SceneManager.LoadScene(1);
		});

		QuizButton.onClick.AddListener(() =>
		{
			SceneManager.LoadScene(2);
		});

        ViewModeButton.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(5);
        });

        QuitButton.onClick.AddListener(() => 
        {
            Application.Quit();
        });
	}
}
