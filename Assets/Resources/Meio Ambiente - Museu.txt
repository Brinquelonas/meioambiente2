Nome da obra	Autor	nome da imagem
Dama com Arminho	Leonardo Da Vinci	DaVinci-DamacomArminho
Homem Vitruviano	Leonardo Da Vinci	DaVinci-HomemVitruviano
A Última ceia 	Leonardo Da Vinci	DaVinci-AUltimaCeia
Mona Lisa	Leonardo Da Vinci	DaVinci-MonaLisa
Best Friends	Romero Britto	RomeroBritto-BestFriends
Cavalo Cavalo	Romero Britto	RomeroBritto-CavaloCavalo
Britto Garden	Romero Britto	RomeroBritto-BrittoGarden
Mona Cat	Romero Britto	RomeroBritto-MonaCat
O Mamoeiro	Tarsila do Amaral	TarsilaDoAmaral-OMamoeiro
Morro da Favela	Tarsila do Amaral	TarsilaDoAmaral-MorroDaFavela
Operários	Tarsila do Amaral	TarsilaDoAmaral-Operarios
Abaporu	Tarsila do Amaral	TarsilaDoAmaral-Abaporu
Auto-Retrato	Van Gogh	VanGogh-AutoRetrato
Os Comedores de Batata	Van Gogh	VanGogh-OsComedoresDeBatata
Lírios	Van Gogh	VanGogh-Lirios
A Noite Estrelada	Van Gogh	VanGogh-ANoiteEstrelada
Guernica	Pablo Picasso	Picasso-Guernica
O Velho Guitarrista Cego	Pablo Picasso	Picasso-OVelhoGuitarristaCego
A Mulher Que Chora	Pablo Picasso	Picasso-AMulherQueChora
Dora Maar com Gato	Pablo Picasso	Picasso-DoraMaarComGato
Senhor dos Passos	Aleijadinho	Aleijadinho-SenhordosPassos
Coroação de Espinhos	Aleijadinho	Aleijadinho-CoroaçãoDeEspinhos
Anjo com o Cálice da Paixão	Aleijadinho	Aleijadinho-AnjoComOCaliceDaPaixao
Senhora das Dores	Aleijadinho	Aleijadinho-SenhoraDasDores