Casa	Tipo	Pergunta	Resposta errada 1	Resposta errada 2	Resposta errada 3	Resposta certa	NPC
	txt	O que é melhor para o meio ambiente?	Carros de luxo	Motos de estrada	Caminhões	Carro movido a biodiesel	1
	txt	Qual destes é um ato que ajuda o meio ambiente?	Jogar lixo no chão	Subir em árvores	Dormir cedo	Plantar uma árvore	1
	txt	A coleta seletiva comum é dividida em:	Lixo e não lixo	Limpo e sujo	Orgânico, papel e resto	Plástico, papel, vidro, e metal	1
	txt	Por que não devemos demorar demais no banho?	Pra sobrar mais tempo pra brincar	Porque outras pessoas podem estar esperando	Porque estraga o chuveiro	Para não desperdiçar água	1
	txt	Por que não devemos esquecer as luzes de casa acesas ao sair?	Porque as luzes acesas podem atrapalhar os vizinhos	Porque as luzes acesas esquentam a casa	Porque seus familiares vão ficar bravos	Para não desperdiçar energia	1
	txt	Por que brincar com fogo é errado?	Porque faz fumaça	Porque desperdiça fósforos		Porque pode causar acidentes	1
	txt	Jogar lixo no chão é...	Bom para o meio ambiente	Desperdício	Legal	Falta de educação e polui o meio ambiente	1
	txt	Qual a forma correta de se escovar os dentes?	De ponta cabeça	Com a torneira aberta o tempo todo	Com duas escovas de dente	Usando água apenas no final para enxaguar a boca	1
	txt	Por que jogar lixo no chão é errado?	Porque o correto é enterrar no jardim mais próximo	Porque o certo é jogar no rio		Porque polui o meio ambiente	1
	txt	Qual destas casas é boa para o meio ambiente?	Casas com 10 janelas	Casa com quintal	Casa com dois andares	Casa com cisterna para reaproveitar água da chuva	1
	txt	Qual destas casas é boa para o meio ambiente?	Casas com 10 janelas	Casa com quintal	Casa com dois andares	Casa com sistema de aquecimento solar	1
	txt	Como devemos tratar os idosos?	Com irritação	Como estranhos	Como desconhecidos	Com respeito	1
	txt	Sempre que pudermos devemos....	Tomar banhos longos	Jogar lixo no chão	Brincar na chuva	Dar nosso lugar no ônibus a idosos ou gestantes	1
	txt	Sempre que pudermos devemos....	Tomar banhos longos	Jogar lixo no chão	Brincar na chuva	Auxiliar pessoas portadoras de deficiência	1
	txt	Na coleta seletiva, embalagem de balas e doces vão no lixo:	De metal	De vidro	Nenhum deles	De Plástico	5
	txt	Na coleta seletiva, folhas de papel do caderno vão no lixo:	De metal	De vidro	Nenhum deles	De Papel	5
	txt	Na coleta seletiva, o lixo vermelho representa:	Vidro	Metal	Papel	Plástico	5
	txt	Na coleta seletiva, o lixo verde representa:	Papel	Metal	Plático	Vidro	5
	txt	Na coleta seletiva, o lixo azul representa:	Vidro	Metal	Plástico	Papel	5
	txt	Na coleta seletiva, o lixo amarelo representa:	Papel	Vidro	Plástico	Metal	5
	txt	É uma boa ação...	Tomar banhos longos	Deixar brinquedos espalhados no chão	Jogar lixo no chão	Doar brinquedos que você não usa mais	5
	txt	Atravessar a rua fora da faixa é...	Divertido	Lei	O correto a se fazer	Errado e perigoso	5
	txt	Antes de atravessar a rua devemos:	Olhar a outra calçada	Colocar uma venda	Dar três pulinhos	Olhar ambos os lados	5
	txt	É muito errado:	Pular corda	Dar seu lugar a outras pessoas no ônibus	Doar brinquedos velhos	Maltratar animais	5
	txt	Faz parte de nossa responsabilidade...	Pular corda	Jogar lixo no chão	Deixar brinquedos espalhados no chão	Não esquecer de alimentar os animais de estimação	5
	txt	Arrancar flores e folhas de árvores e jardim é:	Divertido	Importante para faze-las crescerem mais saudáveis	Uma boa ação	Muito errado	5
	txt	O que deve ser feito ao encontrar uma carteira no chão?	Pegar ela pra você	Entregar para qualquer estranho	Jogar no lixo reciclável	Entregar a um policial	5
	txt	O que deve ser feito com o lixo de algo que você acabou de consumir?	Devemos guardar como recordação	Devemos consumir o lixo também	Deve ser jogado no chão	Deve ser jogado no lixo, seguindo as regras da coleta seletiva	5
12	int					planetario	
16	int					museu	
	txt	Tipo de lixo que deriva de um organismo vivo ou não utiliza produto químico para ser cultuvado	Lixo tóxico	Lixo livre	Lixo reciclável	Lixo orgânico	
	txt	O que é a reciclagem?	Separar o lixo em plástico, metal, vidro, orgânico e papel	Deixar o lixo em qualquer lugar	Entregar o lixo para os coletores	Processo de transformação dos materiais que podem voltar para o seu estado original ou se transformar em outro produto.	
	txt	É transformado em pequenos grânulos, que são utilizados no processo de produção de novos produtos, como: sacos de lixo, embalagens, etc.	Papel	Metal	Vidro	Plástico	
	txt	É triturado e entra no processo industrial para se transformar em novos produtos: jornal, papel higiênico, guardanapos, etc.	Lixo orgânico	Metal	Plástico	Papel	
	txt	São prensadas, fundidas, entram no processo de lingotamento (lingotes fundidos em tiras) e então transformados em bobinas de alumínio. Após é realizada a fabricação novas latinhas.	Qualquer metal	Garrafinhas de vidro	Copos plásticos	Latas	
	txt	No Brasil, qual a cor representa a coleta seletiva do papel/papelão	Vermelho	Preto	Verde	Azul	
	txt	No Brasil, qual a cor representa a coleta seletiva do Plástico	Laranja	Branco	Verde	Vermelho	
	txt	No Brasil, qual a cor representa a coleta seletiva do Vidro	Roxo	Amarelo	Azul	Verde	
	txt	No Brasil, qual a cor representa a coleta seletiva do metal	Cinza	Azul	Branco	Amarelo	
	txt	No Brasil, qual a cor representa a coleta seletiva dos resíduos orgânicos	Vermelho	Amarelo	Verde	Marrom	
	txt	No Brasil, qual a cor representa a coleta seletiva dos resíduos perigosos	Preto	Azul	Amarelo	Laranja	
	txt	No Brasil, qual a cor representa a coleta seletiva da madeira	Marrom	Roxo	Verde	Preto	
	txt	No Brasil, qual a cor representa a coleta seletiva dos resíduos dos serviços de saúde	Cinza	Cor de rosa	Roxo	Branco	
	txt	No Brasil, qual a cor representa a coleta seletiva dos resíduos radioativos	preto	Amarelo	Vermelho e azul	Cinza	
	txt	Quanto tempo o vidro leva para se decompor na natureza?	500 anos	  300 anos	3000 anos	 O vidro não é biodegradável.	
	txt	Assinale a alternativa que aponta corretamente os objetos que NÃO podem ser reciclados.	Garrafa PET; papel sulfite; cacos de vidro; lata de refrigerante.	Todos os materiais podem ser reciclados	Embalagem de xampu; jornal; copo de requeijão (vidro); lacre.	 Embalagem de salgadinho (plástico); papel celofane; prato Duralex (vidro); esponja de aço.	
	txt	Qual é o cuidado a ser tomado antes de enviar um determinado material para a reciclagem?	  Cortá-lo em pedaços pequenos. 	Colocá-lo numa sacola.	Não deve haver cuidados especiais.	Limpá-lo.	
	txt	Quais são algums das consequências mais diretas do despejo inadequado do lixo?	  Não há consequências. 	Esgotamento dos reservatórios de água no planeta e estiagem.	  Chuvas ácidas e o buraco na camada de ozônio. 	Enchentes e doenças, poluição marítima, que causa a morte de diversos peixes e tartarugas.	
	txt	Qual é o país número 1 em reciclagem de alumínio?	Paris	Venezuela	Alemanha	Brasil	
	txt	Que tipo de material gera mais resíduos no Brasil, com 52%?	Metal Em seguida temos o papel, com 26%.	Vidro Em seguida temos o papel, com 26%."	Plástico Em seguida temos o papel, com 26%."	Matéria orgânica. Em seguida temos o papel, com 26%.	
	txt	Quais dos materiais abaixo podem gerar algum tipo de combustível?	Óleo e metal.	Óleo de cozinha e papel	Nenhum material pode gerar combustível	Plástico e óleo de cozinha.	
	txt	Que tipos de resíduos biológicos são prejudiciais à saúde e ao meio ambiente?	Mercúrio, lâmpadas, termômetros, medicamentos vencidos;	Resíduos de cozinha, restos de alimentos, entulho de obras;	Lixo Orgânico, lixo dos banheiros e lixo de construções	Bolsas de sangue, material perfurocortante, sangues, etc.	
	txt	Qual a importância da reciclagem?	Não contribuir para natureza;	Produzir mais materiais	  Derramar mercúrio na água. 	Ter um planeta saudável	
	txt	O que causa, com maior frequência, a contaminação do lençol freático?	  Poluição no ar	  Poluição sonora	Poluição solar	Poluição do solo	
	txt	O que são rejeitos radioativos?	São resíduos produzidos pela decomposição orgânica;	  É o resultado da contaminação do solo; 	São rejeitos dos raios solares	É toda e qualquer matéria resultante de atividade humana que contenha radionuclídeo	
	txt	Aumenta a probabilidade de ocorrer a desertificação?	A ocorrência d tempestades de areia	Mau trato do solo e aumento de chorume em hospitais	Aumento populacional e mortalidade infantil	Desmatamento e imprudência no trato com o solo	
	txt	O que é desequilíbrio ambiental?	  Interação com o meio, tornando-o produtivo; 	É benéfico para atmosfera terrestre.	É a migração de animais 	Emissão desenfreada de gases, crescimento populacional, lixo, resíduos radioativos, ameaça nuclear	
	txt	O que é Desenvolvimento Sustentável?	É a expanção do meio ambiente	Capacidade de expandir-se no meio esquecendo do futuro;	Incapacidade de interagir com o meio e sustentar o futuro.	 Capacidade de interagir com o meio no presente contribuindo com o futuro;	
	txt	Quais são as formas de degradação da Terra?	Água tratada, aterros clandestinos, pesticidas, meio ambiente;	Crescimento da população	  Indústrias, pesticidas, água tratada, queimadas, saneamento, lixo reciclado. 	Queimadas, desmatamentos, industrias, aterros clandestinos, pesticidas, veículos movidos a combustíveis fósseis	
	txt	Quais os grupos de resíduos encontrados nos Estabelecimentos de Assistência à Saúde (EAS)?	Biológicos, comuns, chorume, radioativos, químicos;	Luminosos, químicos radioativos;	Metais e vidros	Radioativos, químicos, comuns, biológicos	
	txt	O que é reciclagem?	“Jogar fora” o lixo produzido.	Coletar todo tipo de material existente em lixos recicláveis.	Nome dado para todo o processo do lixo após seu descarte.	Processo de transformação de materiais usados em novos produtos para consumo.	
	txt	Como separar corretamente seu lixo?	Juntar tudo na lixeira, pois os prédios já fazem o trabalho de separação.	Deixar plásticos sujos junto com lixo orgânico.	Juntar todo tipo de lixo e descartar em ponto de coleta	Separar o lixo orgânico (restos de alimentos, papel sujo e lixo sanitário) dos resíduos sólidos (como plástico, vidro, papel, metal e embalagens longa vida).	
	txt	O que é coleta seletiva?	Destinação de resíduos para lixões e aterros.	Processo de envio de todo o lixo produzido para cooperativas ou entrega para catadores de rua.	A escolha aleatória do melhor lixo produzido.	Processo de separação e recolhimento dos resíduos para o reaproveitamento por meio de reciclagem.	
	txt	O que fazer com o lixo eletrônico – pilhas, baterias e equipamentos quebrados?	Recolher, organizar e armazenar em casa o máximo de tempo que der.	Juntar com plásticos e metais.	Jogar no lixo comum.	Procurar locais específicos para seu descarte.	
	txt	Uma das formas de colaborar com a preservação do meio ambiente é reduzir a produção de resíduos. Mas como?	Optando pela compra de produtos com embalagens recicláveis.	Reutilizando os materiais e objetos sempre que possível.	Apoiando iniciativas de reciclagem.	Todas as anteriores.	
	txt	Como consumir de forma consciente?	Trocando todos os nossos objetos sempre que um novo do mesmo tipo for lançado.	Usar a mangueira para lavar o quintal e o carro.	Adquirindo qualquer tipo de produto se for barato.	Utilizando os recursos naturais para satisfazer nossas necessidades e das gerações futuras.	
	txt	Como preservar árvores e florestas?	Construindo uma casa na árvore.	Reutilizando metais e vidros.	Indo em parques.	Reciclando papéis, jornais e revistas.	
	txt	Qual dos gases abaixo não é conhecido como um dos gases do efeito estufa (GEE)?	N2O – óxido nitroso	CO2 – dióxido de carbono ou gás carbônico	CH4 – metano	O2 – oxigênio	
	txt	Qual dos elementos abaixo não é utilizado como fonte de energia?	Água corrente	Petróleo	Sol	Barra de ferro	
	txt	Qual alternativa apresenta uma vantagem da energia solar?	Não é renovável	É eficaz em qualquer clima.	É disponível a todo momento	Não polui	
	txt	Quais animais abaixo não pertencem a fauna catarinense:	Capivara e Gralha Azul	Preá e Lobo Guará;	Canário da Terra e Pintassilgo;	Periquito Australiano e Urso;	
	txt	Assinale a afirmativa correta:	Fauna é o conjunto de vida animal incluindo-se nela também os periquitos e sabiás	O papagaio é uma ave que possui inteligência assim como os humanos, pois cantam e falam normalmente	O mico-leão dourado é um animal que viveu somente na mata atlântica e foi extinto em 2008. 	Biodiversidade são somente alguns animais, não incluindo os vegetais	
	txt	Sobre a fauna é correto afirmar:  	É permitido ter animais silvestres em cativeiro, como tucanos e araras sem autorização do órgão ambiental competente;	Os maus tratos aos animais é permitido;	Os maus tratos aos animais é bom para o meio ambiente.	É permitido ter animais domésticos em cativeiro, como cachorros e gatos;	
	txt	Qual desses animais é o símbolo do Bioma Mata Atlântica:	Mico-leão dourado	Capivara	Tamanduá Bandeira	Gralha Azul.	
	txt	Nos rios, os peixes normalmente se reproduzem em um único período do ano. Como é chamado este período de reprodução dos peixes?	Período de Juracema	Período de Guataparema	Período de Iracema;	Período de Piracema	
	txt	Assinale a alternativa que não está relacionada com o tema “sustentabilidade”	Uso de fontes de energia solar e eólica;	Reciclagem de resíduos;	Preservação de áreas verdes;	Exploração de recurso florestal (mata) sem controle;	
	txt	Para ter um cachorro como animal de estimação devo:	Prender o cão em uma corrente pequena;	Fazer o devido registro do cão junto ao IBAMA e à Polícia Militar Ambiental, pois se trata de um animal doméstico;	Deixar o animal solto na rua a mercê de ser atropelado por um veículo;	Dar um abrigo, alimentação e carinho;	
	txt	O Estado de Santa Catarina está inserido em qual desses Biomas?	Cerrado	Caatinga	Amazônia	Mata Atlântica	
	txt	Como é chamada a vegetação que acompanha as margens de um rio, que é extremamente importante para a manutenção das boas condições ambientais do ecossistema desse rio?	Floresta	Cerrado	Campo	Mata ciliar	
	txt	Qual a principal causa do aquecimento global:	A produção agropecuária	A recuperação de áreas degradadas	O consumo de água	Emissão de gases poluentes;	
	txt	A poluição dos rios pode dar-se por:	pelo plantio de mudas de árvores nativas	pela fumaça oriunda das chaminés das indústrias	pelo lançamento de esgotos tratados	lançamento de esgotos e dejetos de animais não tratados	
	txt	Quando da retirada da mata ciliar, é incorreto afirmar:	Causa impactos indiretos 	Causa o assoreamento dos rios	Possibilita a poluição das nascentes, pois se retira sua proteção natural	Possibilita o ganho econômico nas propriedades, pois aumenta-se a área produtiva, e não causa danos ao meio ambiente	
	txt	O bioma mais rico em diversidade do planeta fica no Brasil. Ele ocupa 15% do território nacional, mas 93% de sua área original já foi devastada. Que bioma é esse?	Campos	Cerrado	Amazônia	Mata Atlântica	
	txt	Energia elétrica produzida a partir da ação dos ventos denominamos de:	Energia Hidrelétrica 	Energia Nuclear 	Energia solar	Energia Eólica	
	txt	De acordo com a legislação ambiental, assinale a alternativa incorreta acerca das cores das lixeiras para triagem de resíduos:	Vermelha para plástico e Azul para papel	Verde para vidro e Amarelo para metal	Marrom para orgânicos e Cinza para rejeitos	Azul para plástico e Marrom para orgânicos	
	txt	Assinale a afirmativa errada:	Restos de comidas e plantas mortas podem ser transformadas em adubo orgânico	O lixo hospitalar não pode ser recolhido junto com lixo doméstico	Na categoria “lixo seco” podemos incluir o plástico e o vidro, mas não o papel higiênico usado	A categoria “lixo seco” enquadra-se como o lixo que ainda não foi molhado	
	txt	Assinale o item que não pode ser reciclado:	Garrafa de refrigerante	Cadernos	Latinhas de cerveja	Lixo hospitalar	
	txt	Assinale a alternativa adequada. Ao enterrar um animal no solo sem impermeabilização (revestimento) posso: 	Deixar urubus sem alimentação	Causar poluição visual	Aumentar o odor na atmosfera	Estar assumindo o risco de contaminação do lençol freático	
	txt	As "queimadas" realizadas nos campos quando acaba o inverno apresentam como desvantagem:	Enriquecimento de nutrientes no solo	Diminuição da quantidade de fumaça na atmosfera	Aumento da quantidade de água nos campos queimados	A morte de animais e vegetais, empobrecimento do solo	
	txt	Os  desmoronamentos  nas  encostas  ocorridos  em  épocas  de  fortes  chuvas,  geralmente são provocados por:	Buracos abertos para fixar os postes	Pelo excesso de carga dos caminhões que passam pelo local	Grande quantidade de árvores num lugar só	Ocupação irregular dos terrenos e retirada da vegetação em locais de declividade	
	txt	Qual é o meio utilizado para combater pragas de forma natural?	Uso de defensivo agrícola com pouca toxicidade	Uso de produto químico produzido no Brasil	Uso de Agrotóxico controlado pelo governo	Uso do controle Biológico	
	txt	Se ocorresse um colapso na Terra e os seres humanos desaparecessem, o que ocorreria com a vida no planeta?	O planeta Terra iria se extinguir, pois é o homem através da tecnologia que mantém o mesmo em constante evolução	Nada. Tudo continuaria como está	Nenhuma das alternativas esta correta	Haveria a readaptação das demais espécimes animais, seleção natural e reequilíbrio	
	txt	Qual dos elementos naturais abaixo é importante para conservação da umidade do solo e do ar, proteção das nascentes e rios?	Rochas	Água	Madeira	Vegetação	
	txt	No inverno, vou para lugares com o clima mais ameno. Minha chegada de volta anuncia a primavera. Quem sou?	Ganso	Papagaio	Pica-Pau	Andorinha	
	txt	Comemos sementes, brotos e grãos.O cantodo  macho adulto é o despertador do homem do campo. Quem sou?	Gavião	Perú	Cabra	Galinha	
	txt	Relação ecológica desarmônica é:	Quando a relação ocorre entre espécies diferentes	Quando a relação ocorre entre seres da mesma espécie	Quando todos os indivíduos vivem em paz	  Onde ambos indivíduos se prejudicam,ou quando apenas um terá proveito, prejudicando o outro indivíduo 	
	txt	Exemplo de protocooperação:	Lombriga se alimentando no intestino 	Luta entre leões 	Onça matando e comendo uma capivara	Pássaro catando carrapatos do boi	
		O Pica-Pau pode dar quantas bicadas por minuto em uma árvore?	10	1000	300	100	
		Quantas vezes o Beija-Flor bate as asas por segundo?	1500	230	320	105	
		O filhote de um boi recebe 3 nomes diferentes. Quais são eles?	 leitão, novilho ou pônei	boizinho, bezerro ou mini boi	Boizinho, bezerrinho, muzinho	Bezerro, novilho ou vitelo	
		Uma centopéia percorre 100 metros em:	 8 minutos e 40 segundos	1 hora	12 minutos e 10 segundos	3 minutos e 25 segundos	
		O touro fica furioso ao ver a capa de que cor sendo sacudida?	nenhuma das cores, ele não se irrita	vermelha	Preta	Qualquer cor, ele não as distingue	
		O que nunca para de crescer em um roedor?	Pelos	Cauda	Agilidade	Dentes	
		Um ovo de avestruz equivale a que quantidade de ovos de galinha?	12 ovos de galinha	6 ovos de galinha	26 ovos de galinha	2 dúzias (24 ovos de galinha)	
		Como se chama um especialista em pássaros?	Apicultor  	Cinófilo	Passarinheiro	Ornitólogo	
		Quantas vezes o coração de uma girafa é maior do que de uma pessoa?	60 vezes	10 vezes	5 vezes	43 vezes	
		Qual destas árvores tem origem brasileira?	Laranjeira	Abacateiro	Limoeiro	Pitangueira	
		Qual destas plantas frutíferas é uma trepadeira e tem origem brasileira?	Pé de batata	Pé de Melão	Pé de tamarindo	Pé de maracujá	
		Qual das árvores abaixo é brasileira e dá seus frutos no tronco?	Macieira	Ameixeira	Jaqueira	Jabuticabeira	
		Qual planta abaixo produz uma fruta e tem origem no cerrado brasileiro?	Pé de batata	Soja	 Pé de laranja	Pé de abacaxi	
		Qual fruta abaixo é brasileira?	Maçã	Framboesa	Uva	Goiaba	
		Quais frutas a seguir são originárias da Amazônia?	 Banana, maçã e pêssego.	 Jabuticaba, umbú e pêra.	 Abacaxi, caqui e acerola	Açaí, guaraná e cupuaçu.	
		O Cacaueiro é uma árvore de origem brasileira na qual é produzido o cacau. De qual bioma brasileiro é originário o cacaueiro?	Caatinga	Serrado	Mata Atlântica	Amazônia	
		O que é Bioma Brasileiro?	São todas as regiões de um estado	É o conjunto de florestas preservadas do Brasil	São regiões que diferem pelo clima e cegetação	São regiões que compreendem grandes ecossistemas constituídos por uma comunidade biológica com características semelhantes.	
		Quantos Biomas temos no Brasil?	3 tipos de Biomas	9 tipos de Biomas	12 tipos de Biomas	6 tipos de Biomas	
		Quais são os tipos de Biomas do brasil	Vegetação, rio e animais	Planície, planalto e montanhas	Mata atlântica, Pantanal, Rio São Francisco e Serra do rio do rastro	Cerrado, Amazônia, Caatinga, Mata Atlântica, Pantanal, Pampa.	
		Quais os  principais representantes da fauna Brasileira?	Rio São Francisco, Rio Amazonas e Rio Tocantins	Mata Atlântica, Pantanal, Serrado e Vale	Mico leão dourado, Onça-pintada, Capivaras e jacaré	Onça-pintada, boto-cor-de-rosa, arara-azul, capivara, tatu e cobras, como a cascavel e a jararaca	
		Considerado o segundo maior bioma da América Latina e do Brasil. Conhecido como savana brasileira, apresenta grande biodiversidade e compreende uma área de elevado potencial aquífero. 	Bioma Caatinga	Bioma Amazônia	Bioma Pampa	Bioma Cerrado	
		Compreende cerca de 11% do território brasileiro, ocupando boa parte da Região Nordeste até a porção norte de Minas Gerais. O nome dado a esse bioma tem origem indígena e significa “floresta branca”, denominação que remete às características dessa vegetação ao longo da estação seca.	Bioma Pantanal	Bioma Amazônia	Bioma Pampa	Bioma Caatinga	
		Compreende uma área na qual se encontra a maior floresta tropical do mundo.	Bioma Pantanal	Bioma Caatinga	Bioma Pampa	Bioma Amazônia	
		Considerado uma das maiores planícies alagadas do mundo. É o menor bioma em extensão territorial do Brasil, ocupando cerca de 2% do território nacional. É um bioma com grande biodiversidade.	Bioma Cerrado	Bioma Caatinga	Bioma Pampa	Bioma Pantanal	
		Conhecido também como Campos Sulinos, ocupa cerca de 2% do território brasileiro, abrangendo o território do estado do Rio Grande do Sul.	Bioma Cerrado	Bioma Caatinga	Bioma Amazônia	Bioma Pampa	
		Ocupa cerca de 13% do território brasileiro e compreende a região costeira do Brasil. Esse bioma é composto por variados ecossistemas florestais e por uma biodiversidade semelhante à do bioma Amazônia. 	Bioma Cerrado	Bioma Caatinga	Bioma Amazônia	Bioma Mata Atlântica	
		Qual das frutas é brasileira originária do Nordeste?	Limão	Graviola	Morango	Cajú	
		Qual das frutas abaixo é brasileira da Mata Atlântica?	Lichia	kiwi	Laranja	Cambuci	
		Define-se  como um conjunto de normas, princípios, preceitos, costumes, valores que norteiam o comportamento do indivíduo no seu grupo social.	Grupo de valores	Moral	Identidade	Ética	
		O que é cidadania de modo simplificado ?	 Ato de promover ações que alegrem o administrador da cidade	Deixar a cidade limpa e pronta para receber turistas	Respeitar seus deveres	Possuir direitos e deveres	
		Em que ano foi aprovada a Declaração Universal dos Direitos Humanos, e quem foi quem aprovou esta declaração?				Foi aprovada no ano de 1948, pela assembléia geral da Organização das Nações Unidas (ONU).	
		Quais os direitos que a Declaração Universal dos Direitos Humanos, da para a sociedade?	Educação, moradia, saúde e liberdade de expressão.	 Saúde publica de boa qualidade a todos.	 Educação, direito ao trabalho, e saúde gratuita	 Igualdade, liberdade de expressão, direito ao trabalho, e á educação, e direito a vida.	
		Qual foi a promessa feita pelos lideres mundiais, após o termino da Segunda Guerra Mundial com a criação dos Direitos Humano?	A proteção das pessoas menos favorecidas	 A liberdade de expressão de todos	A liberdade para a construção de armamentos	 A proteção universal dos direitos humanos	
		O que a Organização das Nações Unidas queria quando criou a Declaração Universal dos Direitos Humanos?	 Montar um sistema de unificação mundial	Deixar com que as pessoas façam o que bem entender.	Manter o individualismo de todos os pais.	Manter a paz e a segurança internacionais.	
		Em qual constituição foram garantidos os Direitos Humanos no Brasil?	 Constituição de 1975.	Constituição de 1945.	Constituição de 1970.	Constituição de 1988.	
		À partir de que idade os direitos dos Idosos começam a valer?	57 anos.	75 anos.	60 anos.	60 anos.	
		Qual é a porcentagem de assentos reservados nos transportes públicos para idosos?	1%	25%	20%	10%	
		Quais países aderiram aos direitos dos Idosos?	Países desenvolvidos.	Países do Sul do mundo.	Todos os países.	Os países participantes da ONU (com exceções)	
		Qual dessas vantagens para idosos no Brasil é verdadeira?	Não paga taxas bancárias.	Viaja sem custo para todo o mundo.	Não paga casa própria.	Não paga transporte público.	
		Em que dia foi fundado o estatuto do idoso?	15 de junho	30 de março	12 de dezembro	1 de Outubro	
		 Carnaval é comemorado em qual mês?	Abril	Março	Janeiro	Fevereiro	
		Em que mês comemoramos a Páscoa?	Julho	Fevereiro	Março	Abril	
		Em qual mês é comemorado o Dia Internacional da Mulher?	Agosto	Maio	Abril	Março	
		Qual o Dia Mundial do Trabalho ?	4 de Julho	15 de Dezembro	15 de Abril	01 de Maio	
		Qual dia é comemorado o dia das crianças?	20 de Julho	10 de Janeiro	15 de Março	12 de outubro	
		Qual o Dia da Consciência Negra?	1 de Maio	20 de Outubro	10 de Maio	20 de Novembro	