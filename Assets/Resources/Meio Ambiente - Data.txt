Casa	Tipo	Pergunta	Resposta errada 1	Resposta errada 2	Resposta errada 3	Resposta certa	NPC
	txt	O que é melhor para o meio ambiente?	Carros de luxo	Motos de estrada	Caminhões	Carro movido a biodiesel	1
	txt	Qual destes é um ato que ajuda o meio ambiente?	Jogar lixo no chão	Subir em árvores	Dormir cedo	Plantar uma árvore	1
	txt	A coleta seletiva comum é dividida em:	Lixo e não lixo	Limpo e sujo	Orgânico, papel e resto	Plástico, papel, vidro, e metal	1
	txt	Por que não devemos demorar demais no banho?	Pra sobrar mais tempo pra brincar	Porque outras pessoas podem estar esperando	Porque estraga o chuveiro	Para não desperdiçar água	1
	txt	Por que não devemos esquecer as luzes de casa acesas ao sair?	Porque as luzes acesas podem atrapalhar os vizinhos	Porque as luzes acesas esquentam a casa	Porque seus familiares vão ficar bravos	Para não desperdiçar energia	1
	txt	Por que brincar com fogo é errado?	Porque faz fumaça	Porque desperdiça fósforos	Porque quem brinca com fogo faz xixi na cam	Porque pode causar acidentes	1
	txt	Jogar lixo no chão é...	Bom para o meio ambiente	Desperdício	Legal	Falta de educação e polui o meio ambiente	1
	txt	Qual a forma correta de se escovar os dentes?	De ponta cabeça	Com a torneira aberta o tempo todo	Com duas escovas de dente	Usando água apenas no final para enxaguar a boca	1
	txt	Por que jogar lixo no chão é errado?	Porque o correto é enterrar no jardim mais próximo	Porque o certo é jogar no rio	Porque precisamos guardar e acumular sujeiras	Porque polui o meio ambiente	1
	txt	Qual destas casas é boa para o meio ambiente?	Casas com 10 janelas	Casa com quintal	Casa com dois andares	Casa com cisterna para reaproveitar água da chuva	1
	txt	Qual destas casas é boa para o meio ambiente?	Casas com 10 janelas	Casa com quintal	Casa com dois andares	Casa com sistema de aquecimento solar	1
	txt	Como devemos tratar os idosos?	Com irritação	Como estranhos	Como desconhecidos	Com respeito	1
	txt	Sempre que pudermos devemos....	Tomar banhos longos	Jogar lixo no chão	Brincar na chuva	Dar nosso lugar no ônibus a idosos ou gestantes	1
	txt	Sempre que pudermos devemos....	Tomar banhos longos	Jogar lixo no chão	Brincar na chuva	Auxiliar pessoas portadoras de deficiência	1
	txt	Na coleta seletiva, embalagem de balas e doces vão no lixo:	De metal	De vidro	Nenhum deles	De Plástico	5
	txt	Na coleta seletiva, folhas de papel do caderno vão no lixo:	De metal	De vidro	Nenhum deles	De Papel	5
	txt	Na coleta seletiva, o lixo vermelho representa:	Vidro	Metal	Papel	Plástico	5
	txt	Na coleta seletiva, o lixo verde representa:	Papel	Metal	Plático	Vidro	5
	txt	Na coleta seletiva, o lixo azul representa:	Vidro	Metal	Plástico	Papel	5
	txt	Na coleta seletiva, o lixo amarelo representa:	Papel	Vidro	Plástico	Metal	5
	txt	É uma boa ação...	Tomar banhos longos	Deixar brinquedos espalhados no chão	Jogar lixo no chão	Doar brinquedos que você não usa mais	5
	txt	Atravessar a rua fora da faixa é...	Divertido	Lei	O correto a se fazer	Errado e perigoso	5
	txt	Antes de atravessar a rua devemos:	Olhar a outra calçada	Colocar uma venda	Dar três pulinhos	Olhar ambos os lados	5
	txt	É muito errado:	Pular corda	Dar seu lugar a outras pessoas no ônibus	Doar brinquedos velhos	Maltratar animais	5
	txt	Faz parte de nossa responsabilidade...	Pular corda	Jogar lixo no chão	Deixar brinquedos espalhados no chão	Não esquecer de alimentar os animais de estimação	5
	txt	Arrancar flores e folhas de árvores e jardim é:	Divertido	Importante para faze-las crescerem mais saudáveis	Uma boa ação	Muito errado	5
	txt	O que deve ser feito ao encontrar uma carteira no chão?	Pegar ela pra você	Entregar para qualquer estranho	Jogar no lixo reciclável	Entregar a um policial	5
	txt	O que deve ser feito com o lixo de algo que você acabou de consumir?	Devemos guardar como recordação	Devemos consumir o lixo também	Deve ser jogado no chão	Deve ser jogado no lixo, seguindo as regras da coleta seletiva	5
12	int					planetario	
16	int					museu	
		O que é a reciclagem?	Separar o lixo em plástico, metal, vidro, orgânico e papel	Deixar o lixo em qualquer lugar	Entregar o lixo para os coletores	Processo de transformação dos materiais que podem voltar para o seu estado original ou se transformar em outro produto.	
		Como preservar árvores e florestas?	Construindo uma casa na árvore.	Reutilizando metais e vidros.	Indo em parques.	Reciclando papéis, jornais e revistas.	
		Qual das alternativas não é um exemplo de reciclagem?	Separar o lixo seco do lixo orgânico	Separação de lixo	Coleta Seletiva	Incineração	
		Não é fonte de energia renovável:	Energia solar passiva	Energia Geotérmica	Energia hidrelétrica	 Energia nuclear	
		O que é Energia renovável?	É a energia que se renova a cada dia.	É aquela que nunca termina.	É a energia produzida pelo homem com a queima de materiais fósseis.	É aquela que vem de recursos naturais que são naturalmente reabastecidos, como sol, vento, chuva, marés e energia geotérmica.	
							
							
		porque devemos lavar as mãos?	para evitar que as unhas cresçam.	para proteger luz do sol.	para refrescar as mãos.	para evitar contato com virus da gripe e demais bactérias.	
							
		quando devemos lavar as mãos? 	antes de jogar futebol.	ao acordar pela manhã	ao tocar nas plantas	antes das refeições e após usar o banheiro.	
							
		ao escovar os dentes estamos prevenindo?	ganho de peso.	dores nas costas	problemas dentários e virus da gripe.	cáries, mau halito e problemas dentários.	
							
		quais alimentos devem ser lavados antes do consumo?	somente as frutas	frutas e cereais.	nenhum, pois todos os alimentos são limpos.	frutas, legumes e vegetais.	
							
		após usar o banheiro devemos?	apenas dar a descarga.	lavar as mãos e se olhar no espelho.	lavar as mãos lentamente.	dar a descarga, tampar o vaso e lavar as mãos.	
							
		são itens de higiene pessoal..	escova de dente, creme dental e meias.	apenas a escova de dentes.	sabonete e shampoo.	Escova de dentes, creme dental e sabonete.	
							
							
		por que é importante manter as unhas curtas?	para evitar coceiras.	para ficarem bonitas.	para evitar que quebrem.	Para evitar germes e bactérias.	
							
		O que fazer com o lixo eletrônico,pilhas, baterias e equipamentos quebrados?	Recolher, organizar e armazenar em casa o máximo de tempo que der.	juntar com plásticos e metais.	Jogar no lixo comum	Procurar um locar correto para descarte.	
							
		Devemos ter o hábito de..	comprar brinquedos novos	Usar roupa de super herói	colocar todos os lixos na lixeira.	Separar o lixo de forma correta	
							
		Qual matéria prima poupamos se reciclarmos papel?	cadernos e livros.	sacos plasticos.	energia.	Árvores	
							
		É uma boa ação...	comprar um animal de estimação	comprar brinquedos novos	fazer uma viagem	adotar um animal.	
							
		Como prevenimos a cárie?	escovando os dentes 	Passando fio dental	diminuindo a ingestão de açucar.	todas as alternativas.	